

class Fixnum
  NUMBER_TO_19 = %w[zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen]
  def in_words
    numbers
  end

  def numbers
    return 'zero' if zero?
    number_string = to_s.reverse
    count = 0
    result = []
    while count < number_string.length
      temp_three = number_string[count..count + 2]
      place = places(count)
      arr_num_str = []
      arr_num_str << num_part_to_string(temp_three.reverse.to_i)
      result << place << arr_num_str unless arr_num_str.all?(&:empty?)
      count += 3
    end
    result.reverse.reject(&:empty?).join(' ')
  end

  def places(count)
    places = ["thousand", "million", "billion", "trillion"]
    return places[(count / 3) - 1] if count > 0
    ''
  end

  def num_part_to_string(num)
    return '' if num.zero?
    return NUMBER_TO_19[num] if num < 20
    ones = nums_below_twenty(num % 100)
    arr_numbers = [hundred(num), tens((num / 10) % 10), ones]
    arr_numbers.reject(&:empty?)
  end

  def nums_below_twenty(num)
    (num > 19 ? single(num % 100) : lonely_one(num % 100))
  end

  def lonely_one(num)
    (num % 10).zero? ? '' : NUMBER_TO_19[num]
  end

  def single(num)
    (num % 10).zero? ? '' : NUMBER_TO_19[num % 10]
  end

  def tens(num)
    number_tens = %w[twenty thirty forty fifty sixty seventy eighty ninety]
    return '' if num < 2
    number_tens[num - 2]
  end

  def hundred(num)
    hundreds = ''
    hundreds = NUMBER_TO_19[num / 100] + " hundred" if num / 100 > 0
    hundreds
  end
end
